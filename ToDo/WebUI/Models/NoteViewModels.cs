﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUI.Models
{
    public class NoteViewModels
    {
    }

    public class NoteViewModel
    {
        [HiddenInput]
        public int Id { get; set; }

        [Display(Name = "Title", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessageResourceType = (typeof(Resources.Resources)), ErrorMessageResourceName = "PropertyValueRequired")]
        public string Title { get; set; }

        [Display(Name = "NoteTextDisplay", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessageResourceType = (typeof(Resources.Resources)), ErrorMessageResourceName = "PropertyValueRequired")]
        [UIHint("NoteText")]
        public string Text { get; set; }

        [HiddenInput]
        public int ToDoID { get; set; }

    }
}