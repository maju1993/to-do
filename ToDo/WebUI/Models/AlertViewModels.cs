﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUI.Models
{
    public class AlertViewModels
    {
    }

    public class AlertViewModel
    {
        [HiddenInput]
        public int Id { get; set; }

        [Display(Name = "AlertTime", ResourceType = typeof(Resources.Resources))]
        public DateTime Date { get; set; }

        [HiddenInput]
        public int ToDoID { get; set; }

        public AlertViewModel()
        {
            this.Date = DateTime.Now;
        }
    }
}