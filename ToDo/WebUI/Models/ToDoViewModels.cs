﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUI.Models
{
    public class ToDoViewModels
    {
    }

    public class ToDoViewModel
    {
        [HiddenInput]
        public int Id { get; set; }

        [Display(Name = "Title", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessageResourceType = (typeof(Resources.Resources)), ErrorMessageResourceName = "PropertyValueRequired")]
        public string Title { get; set; }

        [Display(Name = "Done", ResourceType = typeof(Resources.Resources))]
        public bool Done { get; set; }

        [Display(Name = "Estimation", ResourceType = typeof(Resources.Resources))]
        public TimeSpan? Estimation { get; set; }

        [Display(Name = "StartDate", ResourceType = typeof(Resources.Resources))]
        [DataType(DataType.Date)]
        [UIHint("Date")]
        public Nullable<DateTime> StartDate { get; set; }

        [Display(Name = "FinishDate", ResourceType = typeof(Resources.Resources))]
        [DataType(DataType.Date)]
        [UIHint("Date")]
        public Nullable<DateTime> FinishDate { get; set; }

        public ApplicationUser User { get; set; }

        [HiddenInput]
        [UIHint("Project")]
        public int? ProjectID { get; set; }

        [Display(Name = "Project", ResourceType = typeof(Resources.Resources))]
        public Project Project { get; set; }

        public ICollection<Note> Notes { get; set; }

        public ICollection<Alert> Alerts { get; set; }

        [Display(Name = "Tags", ResourceType = typeof(Resources.Resources))]
        [RegularExpression("^([^,]+(,[^,]+)*)?$", ErrorMessageResourceType = (typeof(Resources.Resources)), ErrorMessageResourceName = "InvalidTagFormat")]
        public string Tags { get; set; }
    }

    public class DashBoardModelView
    {
    
    }


    //public class ToDoListViewModel
    //{
    //    public IEnumerable<ToDo> All { get; set; }

    //    public ToDo New { get; set; }
    //}
}