﻿using System.ComponentModel.DataAnnotations;

namespace WebUI.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required(ErrorMessageResourceType = (typeof(Resources.Resources)), ErrorMessageResourceName = "PropertyValueRequired")]
        [EmailAddress(ErrorMessageResourceType = (typeof(Resources.Resources)), ErrorMessageResourceName = "PropertyIsNotValidEmailAddress", ErrorMessage = null)]
        [Display(Name = "EmailDisplay", ResourceType=typeof(Resources.Resources))]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string Action { get; set; }
        public string ReturnUrl { get; set; }
    }

    public class ManageUserViewModel
    {
        [Required(ErrorMessageResourceType = (typeof(Resources.Resources)), ErrorMessageResourceName = "PropertyValueRequired")]
        [DataType(DataType.Password)]
        [Display(Name = "CurrentPasswordDisplay", ResourceType = typeof(Resources.Resources))]
        public string OldPassword { get; set; }

        [Required(ErrorMessageResourceType = (typeof(Resources.Resources)), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(100, ErrorMessageResourceName="NewPasswordAndConfirmPasswordNotMatchError",ErrorMessageResourceType = typeof(Resources.Resources), MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "NewPasswordDisplay",ResourceType = typeof(Resources.Resources))]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "ConfirmPasswordDisplay",ResourceType = typeof(Resources.Resources))]
        [Compare("NewPassword", ErrorMessageResourceName="PasswordAndConfirmPasswordNotMatchError",ErrorMessageResourceType = typeof(Resources.Resources))]
        public string ConfirmPassword { get; set; }
    }

    public class LoginViewModel
    {
        [Required(ErrorMessageResourceType = (typeof(Resources.Resources)), ErrorMessageResourceName = "PropertyValueRequired")]
        [EmailAddress(ErrorMessageResourceType = (typeof(Resources.Resources)), ErrorMessageResourceName = "PropertyIsNotValidEmailAddress", ErrorMessage = null)]
        [Display(Name = "EmailDisplay",ResourceType = typeof(Resources.Resources))]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = (typeof(Resources.Resources)), ErrorMessageResourceName = "PropertyValueRequired")]
        [DataType(DataType.Password)]
        [Display(Name = "PasswordDisplay",ResourceType = typeof(Resources.Resources))]
        public string Password { get; set; }

        [Display(Name = "RememberMeDisplay",ResourceType = typeof(Resources.Resources))]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required(ErrorMessageResourceType = (typeof(Resources.Resources)), ErrorMessageResourceName = "PropertyValueRequired")]
        [EmailAddress(ErrorMessageResourceType = (typeof(Resources.Resources)), ErrorMessageResourceName = "PropertyIsNotValidEmailAddress", ErrorMessage=null)]
        [Display(Name = "EmailDisplay", ResourceType = typeof(Resources.Resources))]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = (typeof(Resources.Resources)), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(100, ErrorMessageResourceName = "PasswordLengthError", ErrorMessageResourceType = typeof(Resources.Resources), MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "PasswordDisplay", ResourceType = typeof(Resources.Resources))]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "ConfirmPasswordDisplay", ResourceType = typeof(Resources.Resources))]
        [Compare("Password", ErrorMessageResourceName = "PasswordAndConfirmPasswordNotMatchError", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required(ErrorMessageResourceType = (typeof(Resources.Resources)), ErrorMessageResourceName = "PropertyValueRequired")]
        [EmailAddress(ErrorMessageResourceType = (typeof(Resources.Resources)), ErrorMessageResourceName = "PropertyIsNotValidEmailAddress", ErrorMessage = null)]
        [Display(Name = "EmailDisplay", ResourceType = typeof(Resources.Resources))]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = (typeof(Resources.Resources)), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(100, ErrorMessageResourceName = "PasswordLengthError", ErrorMessageResourceType = typeof(Resources.Resources), MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "PasswordDisplay", ResourceType = typeof(Resources.Resources))]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "ConfirmPasswordDisplay", ResourceType = typeof(Resources.Resources))]
        [Compare("Password", ErrorMessageResourceName = "PasswordAndConfirmPasswordNotMatchError", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessageResourceType = (typeof(Resources.Resources)), ErrorMessageResourceName = "PropertyValueRequired")]
        [EmailAddress(ErrorMessageResourceType = (typeof(Resources.Resources)), ErrorMessageResourceName = "PropertyIsNotValidEmailAddress", ErrorMessage = null)]
        [Display(Name = "EmailDisplay", ResourceType = typeof(Resources.Resources))]
        public string Email { get; set; }
    }
}
