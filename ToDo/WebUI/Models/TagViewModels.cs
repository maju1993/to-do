﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUI.Models
{
    public class TagViewModels
    {
    }

    public class TagViewModel
    {
        [HiddenInput]
        public int Id { get; set; }

        [Display(Name = "Name", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessageResourceType = (typeof(Resources.Resources)), ErrorMessageResourceName = "PropertyValueRequired")]
        public string Name { get; set; }

        [Display(Name = "TagToDoCount", ResourceType = typeof(Resources.Resources))]
        public int ToDoCount { get; set; }
    }

}