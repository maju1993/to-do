﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUI.Models
{
    public class ProjectViewModels
    {
        public static String[] Colors = { "DarkGray", "blue", "brown", "Chartreuse", "gold", "green", "azure", "violet", "pink", "red" };
    }

    public class ProjectViewModel
    {
        [HiddenInput]
        public int Id { get; set; }

        [Display(Name = "Name", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessageResourceType = (typeof(Resources.Resources)), ErrorMessageResourceName = "PropertyValueRequired")]
        public string Name { get; set; }

        [Display(Name = "Color", ResourceType = typeof(Resources.Resources))]
        [UIHint("Color")]
        public string Color { get; set; }

        [Display(Name = "DailyTimeAllocated", ResourceType = typeof(Resources.Resources))]
        public TimeSpan? DailyTime { get; set; }

        public string UserID { get; set; }

        public ApplicationUser User { get; set; }

        public List<ToDo> ToDos { get; set; }

        public ProjectViewModel()
        {
        }
    }

}