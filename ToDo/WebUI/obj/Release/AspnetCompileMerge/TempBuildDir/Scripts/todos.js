﻿$(document).ready(function () {
    var browserLang;
    if (navigator.userLanguage) // Explorer
        browserLang = navigator.userLanguage.substr(0, 2);
    else if (navigator.language) // FF
        browserLang = navigator.language.substr(0, 2);
    else
        browserLang = 'en';

    $('.datepicker').datetimepicker({
        minDate: new Date(),
        pickTime: false,
        language: browserLang
    });

    $('.timepicker').datetimepicker({
        pickDate: false,
        useSeconds: false,
        useCurrent: false,
        minuteStepping: 5,
        language: browserLang
    });

    $('.timepicker').on("dp.show", function (e) {
        if (!$(this).find('input').val())
        {
            $(this).data("DateTimePicker").setDate("0");
        }
    });

    $('.datetimepicker').datetimepicker({
        minDate: new Date(),
        language: browserLang
    });

    $('.todo-form .cancel-btn').click(function () {
        $('.todo-form').addClass('hidden');
        $('.show-form-btn').show();
    })
    $('.show-form-btn').click(function () {
        $(this).hide();
        $('.todo-form').removeClass('hidden');
    });

    onCreateToDoSuccess = function onCreateToDoSuccess() {
        $('.todo-form form')[0].reset();
    }

    $('.tomorrow-selector').click(function () {
        var from = new Date();
        var to = new Date();
        to.setDate(from.getDate() + 1);
        $(this).parent().parent().find('.datepicker').data("DateTimePicker").setDate(to);
    })

    $('.today-selector').click(function () {
        $(this).parent().parent().find('.datepicker').data("DateTimePicker").setDate(new Date());
    })

    $('.7days-selector').click(function () {
        var from = new Date();
        var to = new Date();
        to.setDate(from.getDate() + 7);
        $(this).parent().parent().find('.datepicker').data("DateTimePicker").setDate(to);
    })

    $('.estimate-selector').click(function () {
        $(this).parent().parent().find('input').val($(this).val());
    })

    $('.todo-checkbox').change(function () {
        var object = this;
        $.ajax({
            type: "POST",
            url: $(object).data('url'),
            data: { Id: $(object).val(), Done: $(object).prop('checked') },
            success: function () {
            },
            error: function () {
                $(object).prop('checked', false);
            }
        });
    });

    $("#CreateForm").submit(function (event) {
        $.ajax({
            url: $(this).data('action'),
            type: "post",
            data: $(this).serialize(),
            success: function (data) {
                if (data.success)
                {
                    var alert = '<div class="alert alert-success" style="margin-top:10px">';
                    alert += data.message;
                    alert += '<a href="#" class="close" data-dismiss="alert">&times;</a></div>';
                    $(".bs-todo-alerts").append(alert);
                    ReloadDashBoard();
                }
                else
                {
                    $(".todo-form .validation-message").html(data.message);
                }
            },
            error: function () {
                alert('There is error while submit');
            }
        });
        return false;
    });

    function ReloadDashBoard()
    {
        $.ajax({
            url: "ToDos/DashBoard",
            type: "get",
            success: function (data) {
                $("#todo-lists-container").html(data);
            },
            error: function () {
            }
        });
    }

    $("#todo-lists-container #SelectDayContainer").on("dp.change", function (e) {
        window.location = $(this).data('url') + 'day\\' + $(this).find('input').val().replace(/\./g, "-");
    });
})
