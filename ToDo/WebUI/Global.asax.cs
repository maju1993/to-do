﻿using Domain.Concrete;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebUI.Infrastructure;

namespace WebUI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ControllerBuilder.Current.SetControllerFactory(new NinjectControllerFactory());
            ClientDataTypeModelValidatorProvider.ResourceClassKey = "DefaultErrors";
            DefaultModelBinder.ResourceClassKey = "DefaultErrors";
        }

        private void Application_BeginRequest(Object source, EventArgs e)
        {
            HttpApplication application = (HttpApplication)source;
            HttpContext context = application.Context;

            if (context.Request.UserLanguages != null && Request.UserLanguages.Length > 0)
            {
                string culture = Request.UserLanguages[0];
                CultureInfo newCulture = new System.Globalization.CultureInfo(culture);
                if (culture.Contains("pl"))
                {
                    newCulture.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
                }

                Thread.CurrentThread.CurrentCulture = newCulture;
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
            }
        }
    }
}
