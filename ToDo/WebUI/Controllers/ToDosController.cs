﻿using Domain.Abstract;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using WebUI.Models;
using AutoMapper;
using WebUI.AutoMapper;
using System.Globalization;
using System.Net;
using System.Data.Entity;

namespace WebUI.Controllers
{
    [Authorize]
    public class ToDosController : Controller
    {
        private IToDoRepository ToDoRepository;
        private IProjectRepository ProjectRepository;
        private const string TODAY = "Today";
        private const string TOMORROW = "Tomorrow";

        public ToDosController(IToDoRepository toDoRepository, IProjectRepository projectRepository)
        {
            this.ToDoRepository = toDoRepository;
            this.ProjectRepository = projectRepository;
        }

        // GET: ToDos
        public ActionResult Index()
        {
            ViewBag.Header = Resources.Resources.TodaysTodos;


            IQueryable<ToDo> all = ToDoRepository.ToDos(User.Identity.GetUserId());
            ViewBag.Today = all.Where(
                    x => x.FinishDate.HasValue &&
                    DbFunctions.TruncateTime(x.FinishDate.Value) == DateTime.Today
                ).ToList();

            var tomorrow = DateTime.Today.Date.AddDays(1);
            ViewBag.Tomorrow = all.Where(
                    x => x.FinishDate.HasValue &&
                    DbFunctions.TruncateTime(x.FinishDate.Value) == tomorrow
                ).ToList();
            var nextWeek = DateTime.Today.Date.AddDays(7);
            ViewBag.Week = all.Where(
                    x => x.FinishDate.HasValue &&
                    DbFunctions.TruncateTime(x.FinishDate.Value) <= nextWeek && DbFunctions.TruncateTime(x.FinishDate.Value) >= DateTime.Today
                ).ToList();

            ViewBag.NoDate = all.Where(
                    x => !x.FinishDate.HasValue
                ).ToList();

            ViewBag.Projects = ProjectRepository.Projects(User.Identity.GetUserId());
            return View();
        }

        public ActionResult Archive()
        {
            IQueryable<ToDo> all = ToDoRepository.ToDos(User.Identity.GetUserId());
            List<ToDo> model = all.Where(x => x.Done).OrderByDescending(x => x.FinishDate).OrderByDescending(x => x.StartDate).ToList();
            return View(model);
        }

        public PartialViewResult DashBoard()
        {
            IQueryable<ToDo> all = ToDoRepository.ToDos(User.Identity.GetUserId());
            ViewBag.Today = all.Where(
                    x => x.FinishDate.HasValue &&
                    DbFunctions.TruncateTime(x.FinishDate.Value) == DateTime.Today
                ).ToList();

            var tomorrow = DateTime.Today.Date.AddDays(1);
            ViewBag.Tomorrow = all.Where(
                    x => x.FinishDate.HasValue &&
                    DbFunctions.TruncateTime(x.FinishDate.Value) == tomorrow
                ).ToList();
            var nextWeek = DateTime.Today.Date.AddDays(7);
            ViewBag.Week = all.Where(
                    x => x.FinishDate.HasValue &&
                    DbFunctions.TruncateTime(x.FinishDate.Value) <= nextWeek && DbFunctions.TruncateTime(x.FinishDate.Value) >= DateTime.Today
                ).ToList();

            ViewBag.NoDate = all.Where(
                    x => !x.FinishDate.HasValue
                ).ToList();

            ViewBag.Projects = ProjectRepository.Projects(User.Identity.GetUserId());
            return PartialView("_DashBoard");
        }

        // GET: ToDos/Day
        public ActionResult Day(string date)
        {
            IEnumerable<ToDo> model;
            if (String.Equals(date, TODAY, StringComparison.OrdinalIgnoreCase))
            {
                model = ToDoRepository.ToDos(User.Identity.GetUserId()).Where(
                    x => x.FinishDate.HasValue &&
                    DbFunctions.TruncateTime(x.FinishDate.Value) == DateTime.Today
                );
                ViewBag.Date = DateTime.Today.ToShortDateString();
            }
            else if (String.Equals(date, TOMORROW, StringComparison.OrdinalIgnoreCase))
            {
                DateTime dayTomorrow = DateTime.Today.AddDays(1);
                model = ToDoRepository.ToDos(User.Identity.GetUserId()).Where(
                    x => x.FinishDate.HasValue &&
                    DbFunctions.TruncateTime(x.FinishDate.Value) == dayTomorrow
                );
                ViewBag.Date = dayTomorrow.ToShortDateString();
            }
            else
            {
                try
                {
                    DateTime filterDateDay = DateTime.Parse(date,CultureInfo.CurrentCulture.DateTimeFormat);
                    model = ToDoRepository.ToDos(User.Identity.GetUserId()).Where(
                       x => x.FinishDate.HasValue &&
                       DbFunctions.TruncateTime(x.FinishDate.Value) == filterDateDay
                    );
                    ViewBag.Date = filterDateDay.ToShortDateString();
                }
                catch (Exception ex)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Bad Request");
                }
            }
            ViewBag.Projects = ProjectRepository.Projects(User.Identity.GetUserId());
            return View(model.ToList());
        }

        [HttpPost]
        public ActionResult Create([Bind(Include = "Title,Estimation,StartDate,FinishDate,ProjectID,Tags")] ToDoViewModel model)
        {
            if (Request.IsAjaxRequest())
            {
                if (ModelState.IsValid)
                {
                    Mapper.CreateMap<Models.ToDoViewModel, Domain.Entities.ToDo>()
                        .ForMember(dest => dest.ProjectID, opt => opt.MapFrom(src => (src.ProjectID.HasValue && src.ProjectID.Value > 0) ? src.ProjectID : null))
                        .ForMember(dest => dest.Tags, opt => opt.ResolveUsing<ToDoTagResolver>());
                    Domain.Entities.ToDo domainModel = Mapper.Map<ToDoViewModel, Domain.Entities.ToDo>(model);
                    ToDoRepository.Create(domainModel, User.Identity.GetUserId());
                    return Json(new { success = true, message = String.Format(Resources.Resources.ToDoCreatedMessage, model.Title) });
                }
                else
                {
                    var modelStateErrors = this.ModelState.Keys.SelectMany(key => this.ModelState[key].Errors);
                    var validationMessage = Resources.Resources.CreateTodoError + Environment.NewLine;

                    foreach (var modelStateError in modelStateErrors)
                    {
                        validationMessage += modelStateError.ErrorMessage + Environment.NewLine;
                    }
                    return Json(new { success = false, message = validationMessage });
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            Mapper.CreateMap<Domain.Entities.ToDo, ToDoViewModel>();
            ToDoViewModel model = Mapper.Map<Domain.Entities.ToDo, ToDoViewModel>(ToDoRepository.GetToDo(id, User.Identity.GetUserId()));

            model.Alerts = model.Alerts.OrderBy(x => x.Date).ToList();
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            Mapper.CreateMap<Domain.Entities.ToDo, ToDoViewModel>()
                 .ForMember(dest => dest.Tags, opt => opt.MapFrom(src => String.Join(",", src.Tags.Select(x => x.Name))));
            ToDoViewModel model = Mapper.Map<Domain.Entities.ToDo, ToDoViewModel>(ToDoRepository.GetToDo(id, User.Identity.GetUserId()));
            ViewBag.Projects = ProjectRepository.Projects(User.Identity.GetUserId());
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(ToDoViewModel model)
        {
            if (ModelState.IsValid)
            {

                Mapper.CreateMap<Models.ToDoViewModel, Domain.Entities.ToDo>()
                    .ForMember(dest => dest.ProjectID, opt => opt.MapFrom(src => (src.ProjectID.HasValue && src.ProjectID.Value > 0) ? src.ProjectID : null))
                    .ForMember(dest => dest.Tags, opt => opt.ResolveUsing<ToDoTagResolver>());
                Domain.Entities.ToDo domainModel = Mapper.Map<ToDoViewModel, Domain.Entities.ToDo>(model);
                ToDoRepository.Update(domainModel, User.Identity.GetUserId());
                return RedirectToAction("Details", "ToDos", new { id = model.Id });
            }
            ViewBag.Projects = ProjectRepository.Projects(User.Identity.GetUserId());
            return View(model);
        }

        [HttpPost]
        public ActionResult Mark(int Id, bool done)
        {
            var todo = ToDoRepository.ToDos(User.Identity.GetUserId()).First(x => x.Id == Id);
            todo.Done = done;
            ToDoRepository.Update(todo, User.Identity.GetUserId());
            return Json(new { success = true });
        }
    }
}