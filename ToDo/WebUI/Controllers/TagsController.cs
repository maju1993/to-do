﻿using Domain.Abstract;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using WebUI.Models;
using AutoMapper;
using WebUI.AutoMapper;

namespace WebUI.Controllers
{
    [Authorize]
    public class TagsController : Controller
    {
        private ITagsRepository TagsRepository;

        public TagsController(ITagsRepository tagsRepository)
        {
            this.TagsRepository = tagsRepository;
        }

        // GET: Tags
        public ActionResult Index()
        {
            var tags = TagsRepository.Tags(User.Identity.GetUserId()).ToList();
            Mapper.CreateMap<Domain.Entities.Tag, TagViewModel>();
            List<TagViewModel> model = new List<TagViewModel>();
            foreach (var tag in tags)
            {
                var tagViewModel = Mapper.Map<Domain.Entities.Tag, TagViewModel>(tag);
                tagViewModel.ToDoCount = TagsRepository.ToDos(User.Identity.GetUserId(), tagViewModel.Name).Count();
                model.Add(tagViewModel);
            }
            model=model.OrderByDescending(x => x.ToDoCount).ToList();
            return View(model);
        }

        public ActionResult Tag(string name)
        {
            List<ToDo> todos=TagsRepository.ToDos(User.Identity.GetUserId(),name).OrderBy(x=>x.Done).OrderBy(x=>x.FinishDate).OrderBy(x=>x.Title).ToList();
            Mapper.CreateMap<Domain.Entities.ToDo, ToDoViewModel>()
                .ForMember(dest => dest.Tags, opt => opt.MapFrom(src => String.Join(",",src.Tags.Select(x=>x.Name))));
            List<ToDoViewModel> model = new List<ToDoViewModel>();
            foreach(var t in todos)
            {
                model.Add(Mapper.Map<Domain.Entities.ToDo, ToDoViewModel>(t));
            }
            ViewBag.TagName = name;
            return View(model);
        }
    }
}