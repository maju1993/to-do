﻿using Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace WebUI.Controllers
{
    [Authorize]
    public class ReportsController : Controller
    {
        private IReportRepository ReportRepository;

        public ReportsController(IReportRepository reportRepository)
        {
            this.ReportRepository = reportRepository;
        }

        // GET: Reports
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ProjectCountPieReport()
        {
            var data = ReportRepository.ProjectCountPieReportData(User.Identity.GetUserId(), null);
            List<string> colors= new List<string>();
            foreach(var r in data)
            {
                if (r == data.First())
                    continue;
                int i=0;
                foreach (var c in r)
                { 
                    if (i == 2)
                        colors.Add((string)c);
                    i++;
                }
            }
            return Json(new { data=data, colors=colors}, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ProjectTimePieReport()
        {
            var data = ReportRepository.ProjectTimePieReportData(User.Identity.GetUserId(), null);
            List<string> colors = new List<string>();
            foreach (var r in data)
            {
                if (r == data.First())
                    continue;
                int i = 0;
                foreach (var c in r)
                {
                    if (i == 2)
                        colors.Add((string)c);
                    i++;
                }
            }
            return Json(new { data = data, colors = colors }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult TagCountPieReport()
        {
            var data = ReportRepository.TagCountPieReportData(User.Identity.GetUserId(), null);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult TagTimePieReport()
        {
            var data = ReportRepository.TagTimePieReportData(User.Identity.GetUserId(), null);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}