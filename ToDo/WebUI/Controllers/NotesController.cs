﻿using AutoMapper;
using Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Models;

namespace WebUI.Controllers
{
    [Authorize]
    public class NotesController : Controller
    {
        INoteRepository NoteRepository;
        IToDoRepository ToDoReposiotry;

        public NotesController(INoteRepository noteRepository, IToDoRepository toDoReposiotry)
        {
            this.NoteRepository = noteRepository;
            this.ToDoReposiotry = toDoReposiotry;
        }

        public ActionResult Create(int toDoId)
        {
            var model = new NoteViewModel();
            model.ToDoID = toDoId;
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(NoteViewModel model)
        {
            if(ModelState.IsValid)
            {
                Mapper.CreateMap<Models.NoteViewModel, Domain.Entities.Note>();
                Domain.Entities.Note domainModel = Mapper.Map<NoteViewModel, Domain.Entities.Note>(model);
                NoteRepository.CreateNote(domainModel);
                return RedirectToAction("Details", "ToDos", new { id = model.ToDoID });
            }
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            Mapper.CreateMap<Domain.Entities.Note, NoteViewModel>();
            NoteViewModel model = Mapper.Map<Domain.Entities.Note, NoteViewModel>(NoteRepository.GetNote(id));
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(NoteViewModel model)
        {
            if (ModelState.IsValid)
            {
                Mapper.CreateMap<Models.NoteViewModel, Domain.Entities.Note>();
                Domain.Entities.Note domainModel = Mapper.Map<NoteViewModel, Domain.Entities.Note>(model);
                NoteRepository.UpdateNote(domainModel);
                return RedirectToAction("Details", "ToDos", new { id = model.ToDoID });
            }
            return View(model);
        }

        public ActionResult Delete(int toDoId,int id)
        {
            NoteRepository.DeleteNote(id);
            return RedirectToAction("Details", "ToDos", new { id = toDoId });
        }
    }
}