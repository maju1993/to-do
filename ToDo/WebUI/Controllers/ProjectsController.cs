﻿using AutoMapper;
using Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Models;
using Microsoft.AspNet.Identity;
using Domain.Entities;

namespace WebUI.Controllers
{
    [Authorize]
    public class ProjectsController : Controller
    {
        private IProjectRepository ProjectRepository;

        public ProjectsController(IProjectRepository projectRepository)
        {
            this.ProjectRepository = projectRepository;
        }

        // GET: Project
        public ActionResult Index()
        {
            var projects = ProjectRepository.Projects(User.Identity.GetUserId()).OrderBy(x => x.Name).ToList();
            return View(projects);
        }


        // GET: Project/Create
        [HttpGet]
        public ActionResult Create()
        {
            ProjectViewModel model = new ProjectViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(ProjectViewModel model)
        {
            if (ModelState.IsValid)
            {
                Mapper.CreateMap<Models.ProjectViewModel, Domain.Entities.Project>();
                Domain.Entities.Project domainModel = Mapper.Map<ProjectViewModel, Domain.Entities.Project>(model);
                domainModel.UserID = User.Identity.GetUserId();
                ProjectRepository.CreateProject(domainModel);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            Mapper.CreateMap<Domain.Entities.Project, ProjectViewModel>();
            ProjectViewModel model = Mapper.Map<Domain.Entities.Project, ProjectViewModel>(ProjectRepository.GetProject(id, User.Identity.GetUserId()));
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(ProjectViewModel model)
        {
            if(ModelState.IsValid)
            {
                Mapper.CreateMap<Models.ProjectViewModel, Domain.Entities.Project>();
                Domain.Entities.Project domainModel = Mapper.Map<ProjectViewModel, Domain.Entities.Project>(model);
                ProjectRepository.UpdateProject(domainModel);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // Get Project/Details/1
        public ActionResult Details(int id)
        {
            var project = ProjectRepository.Projects(User.Identity.GetUserId()).First(x => x.Id == id);
            Mapper.CreateMap<Domain.Entities.Project, ProjectViewModel>();
            ProjectViewModel model = Mapper.Map<Domain.Entities.Project, ProjectViewModel>(project);


            List<ToDo> todos = project.ToDos.OrderBy(x => x.Done).OrderBy(x => x.FinishDate).OrderBy(x => x.Title).ToList();
            Mapper.CreateMap<Domain.Entities.ToDo, ToDoViewModel>()
                .ForMember(dest => dest.Tags, opt => opt.MapFrom(src => String.Join(",", src.Tags.Select(x => x.Name))));
            List<ToDoViewModel> todoViewList = new List<ToDoViewModel>();
            foreach (var t in todos)
            {
                todoViewList.Add(Mapper.Map<Domain.Entities.ToDo, ToDoViewModel>(t));
            }

            ViewBag.ToDos = todoViewList;
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            ProjectRepository.DeleteProject(User.Identity.GetUserId(), id);
            return RedirectToAction("Index");
        }
    }
}