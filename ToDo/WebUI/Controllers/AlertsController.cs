﻿using AutoMapper;
using Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Models;

namespace WebUI.Controllers
{
    [Authorize]
    public class AlertsController : Controller
    {
        private IAlertRepository AlertRepository;

        public AlertsController(IAlertRepository alertRepository)
        {
            this.AlertRepository = alertRepository;
        }

        // GET: Alert
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create(int toDoId)
        {
            AlertViewModel model = new AlertViewModel();
            model.ToDoID = toDoId;
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(AlertViewModel model)
        {
            if(ModelState.IsValid)
            {
                Mapper.CreateMap<AlertViewModel, Domain.Entities.Alert>();
                Domain.Entities.Alert domainModel = Mapper.Map<AlertViewModel, Domain.Entities.Alert>(model);
                AlertRepository.CreateAlert(domainModel);
                return RedirectToAction("Details", "ToDos", new { id = model.ToDoID });
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            Mapper.CreateMap<Domain.Entities.Alert, AlertViewModel>();
            AlertViewModel model = Mapper.Map<Domain.Entities.Alert, AlertViewModel>(AlertRepository.GetAlert(id));
            model.Date = model.Date.ToLocalTime();
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(AlertViewModel model)
        {
            if (ModelState.IsValid)
            {
                Mapper.CreateMap<Models.AlertViewModel, Domain.Entities.Alert>();
                Domain.Entities.Alert domainModel = Mapper.Map<AlertViewModel, Domain.Entities.Alert>(model);
                AlertRepository.UpdateAlert(domainModel);
                return RedirectToAction("Details", "ToDos", new { id = model.ToDoID });
            }
            return View(model);
        }

        public ActionResult Delete(int toDoId, int id)
        {
            AlertRepository.DeleteAlert(id);
            return RedirectToAction("Details", "ToDos", new { id = toDoId });
        }
    }
}