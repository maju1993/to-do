﻿using Domain.Abstract;
using Domain.Concrete;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace WebUI.Infrastructure
{
    public class NinjectControllerFactory: DefaultControllerFactory
    {
        private IKernel ninjectKernel;

        public NinjectControllerFactory()
        {
            ninjectKernel = new StandardKernel();
            AddBindings();
        }

        protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, Type controllerType)
        {
            return controllerType == null
                ? null
                : (IController)ninjectKernel.Get(controllerType);
        }

        private void AddBindings()
        {
            ninjectKernel.Bind<IToDoRepository>().To<EFToDoReposiotry>();
            ninjectKernel.Bind<IAlertRepository>().To<EFAlertRepository>();
            ninjectKernel.Bind<IProjectRepository>().To<EFProjectReposiotry>();
            ninjectKernel.Bind<INoteRepository>().To<EFNoteRepository>();
            ninjectKernel.Bind<ITagsRepository>().To<EFTagsRepository>();
            ninjectKernel.Bind<IReportRepository>().To<EFReportRepository>();
        }
    }
}