﻿using AutoMapper;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.AutoMapper
{
    public class ToDoTagResolver : ValueResolver<ToDoViewModel, IList<Tag>>
    {
        protected override IList<Tag> ResolveCore(ToDoViewModel viewmodel)
        {
            var tags = new List<Tag>();
            if (string.IsNullOrEmpty(viewmodel.Tags))
                return tags;
            var splitted = viewmodel.Tags.Split(',');
            foreach(var tag in splitted)
                tags.Add(new Tag(){ Name = tag.Trim()});
            return tags;
        }
    }
}