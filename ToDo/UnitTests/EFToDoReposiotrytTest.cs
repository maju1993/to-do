﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Domain.Abstract;
using Domain.Concrete;
using Domain.Entities;

namespace UnitTests
{
    [TestClass]
    public class EFToDoReposiotrytTest
    {
        [TestMethod]
        public void CreateShouldReturnCreatedId()
        {
            IToDoRepository repository = new EFToDoReposiotry();
            ToDo toDo = new ToDo()
            {
                Id = 0,
                Title = "test title",
                Done = true,
                Estimation = TimeSpan.FromHours(1)
            };
            int id = repository.Create(toDo, null);
            Assert.AreNotEqual(0, id);
        }
    }
}
