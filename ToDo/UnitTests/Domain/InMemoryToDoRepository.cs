﻿using Domain.Abstract;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests.Domain
{
    public class InMemoryToDoRepository: IToDoRepository
    {
        private List<ToDo> _db = new List<ToDo>();

        ToDo IToDoRepository.GetToDo(int id, string userId)
        {
            throw new NotImplementedException();
        }

        IQueryable<ToDo> IToDoRepository.ToDos(string userId)
        {
            throw new NotImplementedException();
        }

        int IToDoRepository.Create(ToDo todo, string userId)
        {
            throw new NotImplementedException();
        }

        void IToDoRepository.Update(ToDo todo, string userId)
        {
            throw new NotImplementedException();
        }
    }
}
