﻿using Domain.Abstract;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Concrete
{
    public class EFToDoReposiotry : IToDoRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<ToDo> ToDos(string userId)
        {
            return context.ToDos.Where(x => x.UserID == userId);
        }

        public int Create(ToDo todo, string userId)
        {
            todo.UserID = userId;
            if(todo.Tags!=null)
            { 
                foreach (var tag in todo.Tags)
                {
                    var dbtag = context.Tags.FirstOrDefault(x => x.Name == tag.Name);
                    if (dbtag == null)
                    {
                        tag.ToDos = new List<ToDo>();
                        context.Tags.Add(tag);
                        tag.ToDos.Add(todo);
                    }
                    else
                    {
                        dbtag.ToDos.Add(todo);
                    }
                }
                todo.Tags.Clear();
            }
            context.ToDos.Add(todo);
            context.SaveChanges();
            return todo.Id;
        }

        public void Update(ToDo todo, string userId)
        {
            ToDo dbToDo = context.ToDos.First(x => x.Id == todo.Id && x.UserID == userId);

            var dbTags=context.Tags.Where(x => x.ToDos.Any(t=>t.Id==todo.Id)).ToList();

            foreach (var tag in dbTags)
            {
                tag.ToDos.Remove(dbToDo);
            }
            foreach (var tag in todo.Tags)
            {
                var dbtag = context.Tags.FirstOrDefault(x => x.Name == tag.Name);
                if (dbtag == null)
                {
                    tag.ToDos = new List<ToDo>();
                    context.Tags.Add(tag);
                    tag.ToDos.Add(dbToDo);
                }
                else
                {
                    dbtag.ToDos.Add(dbToDo);
                }
            }


            dbToDo.Title = todo.Title;
            dbToDo.Done = todo.Done;
            dbToDo.Estimation = todo.Estimation;
            dbToDo.StartDate = todo.StartDate;
            dbToDo.FinishDate = todo.FinishDate;
            dbToDo.Project = context.Projects.FirstOrDefault(x => x.Id == todo.ProjectID);
            //dbToDo.Tags = todo.Tags;



            context.SaveChanges();
        }

        public ToDo GetToDo(int id, string userId)
        {
            return context.ToDos.FirstOrDefault(x => x.UserID == userId && x.Id == id);
        }
    }
}
