﻿using Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Concrete
{
    public class SmtpEmailSender : IEmailSender
    {
        private const string SERVER_EMAIL = "todoserwer@gmail.com";
        private const string SERVER_PASSWORD = "!@#qweASD";
        private const string SERVER_HOST = "smtp.gmail.com";
        private const int SERVER_PORT = 587;
        private const bool ENABLE_SSL = true;

        public void SendAlertEmail(Entities.Alert alert)
        {
            using (var message = new MailMessage(SERVER_EMAIL, alert.ToDo.User.Email))
            {
                message.Subject = "Przypomnienie z aplikacji ToDo";
                message.Body = "Witaj - " + alert.ToDo.User.UserName;
                message.Body += Environment.NewLine;
                message.Body += "Data wysłania: " + DateTime.Now;
                message.Body += Environment.NewLine;
                message.Body += "Data przypomnienia: " + alert.Date;
                using (SmtpClient client = new SmtpClient
                {
                    EnableSsl = ENABLE_SSL,
                    Host = SERVER_HOST,
                    Port = SERVER_PORT,
                    Credentials = new NetworkCredential(SERVER_EMAIL, SERVER_PASSWORD)
                })
                {
                    client.Send(message);
                }
            }
        }
    }
}
