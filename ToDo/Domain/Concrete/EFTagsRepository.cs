﻿using Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Concrete
{
    public class EFTagsRepository : ITagsRepository
    {
        private EFDbContext context = new EFDbContext();

        IQueryable<Entities.Tag> ITagsRepository.Tags(string userId)
        {
            return context.ToDos.Where(t => t.UserID == userId).SelectMany(x => x.Tags).Distinct();
        }

        public IQueryable<Entities.ToDo> ToDos(string userId, string tagName)
        {
            return context.Tags.Where(t => t.Name == tagName).SelectMany(x => x.ToDos).Where(x => x.UserID == userId);
        }
    }
}
