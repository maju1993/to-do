﻿using Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Concrete
{
    public class EmailNotification : IAlertNotification
    {
        private IEmailSender sender;

        public EmailNotification(IEmailSender sender)
        {
            this.sender = sender;
        }

        public void sendNotification(Entities.Alert alert)
        {
            sender.SendAlertEmail(alert);
        }
    }
}
