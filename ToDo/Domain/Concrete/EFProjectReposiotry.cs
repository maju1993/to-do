﻿using Domain.Abstract;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Concrete
{
    public class EFProjectReposiotry : IProjectRepository
    {
        private EFDbContext context=new EFDbContext();

        IQueryable<Project> IProjectRepository.Projects(string userId)
        {
            return context.Projects.Where(x => x.User.Id == userId);
        }

        public void DeleteProject(string userId, int projectId)
        {
            var project=context.Projects.First(x =>x.UserID==userId && x.Id == projectId);
            context.ToDos.RemoveRange(project.ToDos);
            context.Projects.Remove(project);
            context.SaveChanges();
        }

        public void CreateProject(Entities.Project project)
        {
            context.Projects.Add(project);
            context.SaveChanges();
        }

        public Project GetProject(int projectId,string userId)
        {
            return context.Projects.First(x => x.Id == projectId && x.UserID == userId);
        }

        public void UpdateProject(Project project)
        {
            var forUpdate = context.Projects.First(x => x.Id == project.Id);
            forUpdate.Name = project.Name;
            forUpdate.Color = project.Color;
            forUpdate.DailyTime = project.DailyTime;
            context.SaveChanges();
        }
    }
}
