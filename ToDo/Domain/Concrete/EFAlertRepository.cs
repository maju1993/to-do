﻿using Domain.Abstract;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Concrete
{
    public class EFAlertRepository : IAlertRepository
    {
        private EFDbContext context = new EFDbContext();

        public IEnumerable<Alert> GetAlertsForNotification()
        {
            return context.Alerts.Include("ToDo").Include("ToDo.User").Where(x => x.Date < DateTime.Now);
        }

        public void DeleteAlert(int Id)
        {
            var alert = context.Alerts.SingleOrDefault(x => x.Id == Id);
            if (alert != null)
            {
                context.Alerts.Remove(alert);
                context.SaveChanges();
            }
        }

        public IQueryable<Alert> Alerts(int todoId)
        {
            return context.Alerts.Where(x => x.ToDoID == todoId);
        }

        public void CreateAlert(Entities.Alert alert)
        {
            alert.ToDo = context.ToDos.First(x => x.Id == alert.ToDoID);
            alert.Date = alert.Date.ToUniversalTime();
            context.Alerts.Add(alert);
            context.SaveChanges();
        }

        public Alert GetAlert(int id)
        {
            return context.Alerts.FirstOrDefault(x => x.Id == id);
        }

        public void UpdateAlert(Alert alert)
        {
            var forUpdate = context.Alerts.First(a => a.Id == alert.Id);
            forUpdate.Date = alert.Date.ToUniversalTime();
            context.SaveChanges();
        }
    }
}
