﻿using Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Concrete
{
    public class EFReportRepository : IReportRepository
    {
        private EFDbContext context = new EFDbContext();

        IEnumerable<IEnumerable<object>> IReportRepository.ProjectCountPieReportData(string userId, bool? done)
        {
            var projects = context.Projects.Where(x => x.User.Id == userId).ToList();
            List<object[]> data = new List<object[]>();
            //pie title
            object[] row = new object[3];
            row[0] = Resources.Resources.Project;
            row[1] = Resources.Resources.TodoCount;
            row[2] = "color";
            data.Add(row);

            if (done == null)
                projects.OrderByDescending(x => x.ToDos.Count());
            else
                projects.OrderByDescending(x => x.ToDos.Where(y=>y.Done==done.Value).Count());

            foreach (var p in projects)
            {
                row = new object[3];
                row[0] = p.Name;
                if (p.ToDos == null)
                    row[1] = 0;
                else
                {
                    if (done == null)
                        row[1] = p.ToDos.Count();
                    else
                        row[1] = p.ToDos.Count(x => x.Done == done.Value);
                }
                row[2] = p.Color;
                data.Add(row);
            }
            return data;
        }

        IEnumerable<IEnumerable<object>> IReportRepository.ProjectTimePieReportData(string userId, bool? done)
        {
            var projects = context.Projects.Where(x => x.User.Id == userId).ToList();
            List<object[]> data = new List<object[]>();
            //pie title
            object[] row = new object[3];
            row[0] = Resources.Resources.Project;
            row[1] = Resources.Resources.TodoCount;
            row[2] = "color";
            data.Add(row);

            foreach (var p in projects)
            {
                row = new object[3];
                row[0] = p.Name;
                if (p.ToDos == null)
                    row[1] = 0;
                else
                {
                    if (done == null)
                        row[1] = TimeSpan.FromMilliseconds(p.ToDos.Where(x => x.Estimation.HasValue).Sum(x => x.Estimation.Value.TotalMilliseconds)).TotalHours;
                    else
                        row[1] = TimeSpan.FromMilliseconds(p.ToDos.Where(x => x.Estimation.HasValue && x.Done == done).Sum(x => x.Estimation.Value.TotalMilliseconds)).TotalHours;
                }
                row[2] = p.Color;
                data.Add(row);
            }
            return data;
        }

        public IEnumerable<IEnumerable<object>> TagCountPieReportData(string userId, bool? done)
        {
            var tags = context.Tags.ToList();
            List<object[]> data = new List<object[]>();
            //pie title
            object[] row = new object[2];
            row[0] = Resources.Resources.Tags;
            row[1] = Resources.Resources.TodoCount;
            data.Add(row);

            foreach (var t in tags)
            {
                row = new object[2];
                row[0] = "#" + t.Name;
                if (t.ToDos == null)
                    row[1] = 0;
                else
                    if (done == null)
                        row[1] = t.ToDos.Where(x => x.UserID == userId).Count();
                    else
                        row[1] = t.ToDos.Where(x => x.UserID == userId && x.Done == done.Value).Count();
                data.Add(row);
            }
            return data;
        }

        public IEnumerable<IEnumerable<object>> TagTimePieReportData(string userId, bool? done)
        {
            var tags = context.Tags.ToList();
            List<object[]> data = new List<object[]>();
            //pie title
            object[] row = new object[2];
            row[0] = Resources.Resources.Tags;
            row[1] = Resources.Resources.TotalTime;
            data.Add(row);

            foreach (var t in tags)
            {
                row = new object[2];
                row[0] = "#" + t.Name;
                if (t.ToDos == null)
                    row[1] = 0;
                else
                    if (done == null)
                        row[1] = TimeSpan.FromMilliseconds(t.ToDos.Where(x => x.Estimation.HasValue && x.UserID == userId).Sum(x => x.Estimation.Value.TotalMilliseconds)).TotalHours;
                    else
                        row[1] = TimeSpan.FromMilliseconds(t.ToDos.Where(x => x.UserID == userId && x.Done == done.Value).Sum(x => x.Estimation.Value.TotalMilliseconds)).TotalHours;
                data.Add(row);
            }
            return data;
        }
    }
}
