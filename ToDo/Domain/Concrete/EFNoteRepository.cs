﻿using Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Concrete
{
    public class EFNoteRepository: INoteRepository
    {
        private EFDbContext context = new EFDbContext();

        public void CreateNote(Entities.Note note)
        {
            note.ToDo = context.ToDos.First(x => x.Id == note.ToDoID);
            note.Created = DateTime.UtcNow;
            context.Notes.Add(note);
            context.SaveChanges();
        }

        public void UpdateNote(Entities.Note note)
        {
            var forUpdate = context.Notes.First(n => n.Id == note.Id);
            forUpdate.Title = note.Title;
            forUpdate.Text = note.Text;
            context.SaveChanges();
        }

        public void DeleteNote(int noteId)
        {
            context.Notes.Remove(context.Notes.First(x => x.Id == noteId));
            context.SaveChanges();
        }

        public Entities.Note GetNote(int noteId)
        {
            return context.Notes.First(x => x.Id == noteId);
        }
    }
}
