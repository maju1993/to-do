﻿using Domain.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Concrete
{
    public class EFDbContext : IdentityDbContext<ApplicationUser>
    {
        public EFDbContext()
            :base("DefaultConnection")
        {
            Database.SetInitializer<EFDbContext>(new EFDbInitializer());
        }

        public static EFDbContext Create()
        {
            EFDbContext context = new EFDbContext();
            return context;
        }

        public DbSet<ToDo> ToDos { get; set; }
        public DbSet<Alert> Alerts { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Note> Notes { get; set; }
    }

    public class EFDbInitializer : DropCreateDatabaseIfModelChanges<EFDbContext>// DropCreateDatabaseAlways<EFDbContext>
    {
        protected override void Seed(EFDbContext context)
        {
            List<ApplicationUser> users = new List<ApplicationUser>(){
                new ApplicationUser(){ UserName="user 1"},
                new ApplicationUser(){ UserName="user 2"},
                new ApplicationUser(){ UserName="user 3"},
                new ApplicationUser(){ Id="1", UserName="maju1993@o2.pl", Email="maju1993@o2.pl",SecurityStamp="bd388cb6-5b2a-49ba-ab6d-6908c28edfd8", PasswordHash="AFnRhvfBqPUMBRJKbdYHifH+J0p5pommEVLMEgpEqoHxYdri9rydzIA5/iTi0XJGiQ=="},
            };
            users.ForEach(x=>context.Users.Add(x));
            context.SaveChanges();

            List<Project> projects = new List<Project>(){
                new Project(){ Name="Praca inżynierska", User=context.Users.FirstOrDefault(x=>x.UserName=="maju1993@o2.pl"), DailyTime=TimeSpan.FromMinutes(300), Color="blue"},
                new Project(){ Name="Dom", User=context.Users.FirstOrDefault(x=>x.UserName=="maju1993@o2.pl"), DailyTime=TimeSpan.FromMinutes(240), Color="Chartreuse"},
                new Project(){ Name="Rodzina", User=context.Users.FirstOrDefault(x=>x.UserName=="maju1993@o2.pl"), Color="gold"}
            };

            projects.ForEach(p=>context.Projects.Add(p));
            context.SaveChanges();

            List<ToDo> todos = new List<ToDo>(){
                new ToDo(){ Title="Konsultacja pracy", Project=context.Projects.FirstOrDefault(x=>x.Name=="Praca inżynierska"), Done=false, FinishDate=DateTime.Now, Estimation=TimeSpan.FromMinutes(90), User=context.Users.FirstOrDefault(x=>x.UserName=="maju1993@o2.pl") },
                new ToDo(){ Title="Stworzenie strony tytyłowej", Project=context.Projects.FirstOrDefault(x=>x.Name=="Praca inżynierska"), Done=true, FinishDate=DateTime.Now, Estimation=TimeSpan.FromMinutes(30), User=context.Users.FirstOrDefault(x=>x.UserName=="maju1993@o2.pl") },
                new ToDo(){ Title="Odebranie obiegówki", Project=context.Projects.FirstOrDefault(x=>x.Name=="Praca inżynierska"), Done=false, FinishDate=DateTime.Now.AddDays(1), Estimation=TimeSpan.FromMinutes(120), User=context.Users.FirstOrDefault(x=>x.UserName=="maju1993@o2.pl") },
                new ToDo(){ Title="Kupienie stołu do kuchni", Project=context.Projects.FirstOrDefault(x=>x.Name=="Dom"), Done=false, FinishDate=DateTime.Now.AddDays(1), Estimation=TimeSpan.FromMinutes(120), User=context.Users.FirstOrDefault(x=>x.UserName=="maju1993@o2.pl") },
                new ToDo(){ Title="Odmalowanie kuchni", Project=context.Projects.FirstOrDefault(x=>x.Name=="Dom"), Done=false, FinishDate=DateTime.Now.AddDays(1), Estimation=TimeSpan.FromMinutes(240), User=context.Users.FirstOrDefault(x=>x.UserName=="maju1993@o2.pl") },
                new ToDo(){ Title="Duże zakupy na impreze", Project=context.Projects.FirstOrDefault(x=>x.Name=="Dom"), Done=false, FinishDate=DateTime.Now.AddDays(1), Estimation=TimeSpan.FromMinutes(90), User=context.Users.FirstOrDefault(x=>x.UserName=="maju1993@o2.pl") },
                new ToDo(){ Title="Kupić prezent siostrze na urodziny", Project=context.Projects.FirstOrDefault(x=>x.Name=="Rodzina"), Done=false, FinishDate=DateTime.Now.AddDays(1), Estimation=TimeSpan.FromMinutes(60), User=context.Users.FirstOrDefault(x=>x.UserName=="maju1993@o2.pl") },
                new ToDo(){ Title="Zawieźć babcię na szczepienie", Project=context.Projects.FirstOrDefault(x=>x.Name=="Rodzina"), Done=false, FinishDate=DateTime.Now.AddDays(1), Estimation=TimeSpan.FromMinutes(180), User=context.Users.FirstOrDefault(x=>x.UserName=="maju1993@o2.pl") }
            };

            todos.ForEach(t => context.ToDos.Add(t));
            context.SaveChanges();

            List<Tag> tags = new List<Tag>(){
                new Tag(){ Name="zakupy" , ToDos= context.ToDos.Where(x=>x.Title=="Kupienie stołu do kuchni" || x.Title=="Kupić prezent siostrze na urodziny").ToList()},
                new Tag(){ Name="samochód", ToDos= context.ToDos.Where(x=>x.Title=="Odebranie obiegówki" || x.Title=="Kupienie stołu do kuchni" || x.Title=="Zawieźć babcię na szczepienie").ToList()}
            };

            tags.ForEach(p => context.Tags.Add(p));
            context.SaveChanges();

            List<Alert> alerts = new List<Alert>(){
                new Alert(){ Date=DateTime.Now.AddSeconds(20), ToDo=context.ToDos.First(x=>x.Id==1)},
                new Alert(){ Date=DateTime.Now.AddSeconds(35), ToDo=context.ToDos.First(x=>x.Id==2)},
                new Alert(){ Date=DateTime.Now.AddSeconds(37), ToDo=context.ToDos.First(x=>x.Id==1)}
            };
            alerts.ForEach(a => context.Alerts.Add(a));
            context.SaveChanges();

            base.Seed(context);
        }
    }
}
