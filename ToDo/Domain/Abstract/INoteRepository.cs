﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Abstract
{
    public interface INoteRepository
    {
        void CreateNote(Note note);

        void UpdateNote(Note note);

        void DeleteNote(int noteId);

        Note GetNote(int noteId);
    }
}
