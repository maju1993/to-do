﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Abstract
{
    public interface ITagsRepository
    {
        IQueryable<Tag> Tags(string userId);

        IQueryable<ToDo> ToDos(string userId, string tagName);
    }
}
