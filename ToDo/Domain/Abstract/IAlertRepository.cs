﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Abstract
{
    public interface IAlertRepository
    {
        IEnumerable<Alert> GetAlertsForNotification();

        IQueryable<Alert> Alerts(int todoId);

        void CreateAlert(Alert alert);

        Alert GetAlert(int id);

        void UpdateAlert(Alert alert);

        void DeleteAlert(int Id);
    }
}
