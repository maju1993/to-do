﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Abstract
{
    public interface IProjectRepository
    {
        IQueryable<Project> Projects(string userId);

        void DeleteProject(string userId, int projectId);

        void CreateProject(Project project);

        void UpdateProject(Project project);

        Project GetProject(int projectId,string userId);
    }
}
