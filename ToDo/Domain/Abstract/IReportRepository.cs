﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Abstract
{
    public interface IReportRepository
    {
        IEnumerable<IEnumerable<object>> ProjectCountPieReportData(string userId, bool? done);

        IEnumerable<IEnumerable<object>> ProjectTimePieReportData(string userId, bool? done);

        IEnumerable<IEnumerable<object>> TagCountPieReportData(string userId, bool? done);

        IEnumerable<IEnumerable<object>> TagTimePieReportData(string userId, bool? done);
    }
}
