﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Abstract
{
    public interface IToDoRepository
    {
        ToDo GetToDo(int id,string userId);

        IQueryable<ToDo> ToDos(string userId);

        int Create(ToDo todo,string userId);

        void Update(ToDo todo, string userId);

    }
}
