﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class ToDo
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        public bool Done { get; set; }

        public TimeSpan? Estimation { get; set; }

        public Nullable<DateTime> StartDate { get; set; }

        public Nullable<DateTime> FinishDate { get; set; }

        [ForeignKey("User")]
        public string UserID { get; set; }

        public virtual ApplicationUser User { get; set; }

        [ForeignKey("Project")]
        public int? ProjectID { get; set; }

        public virtual Project Project { get; set; }

        public virtual ICollection<Note> Notes { get; set; }

        public virtual ICollection<Alert> Alerts { get; set; }

        public virtual List<Tag> Tags { get; set; }

        public ToDo()
        {
            Done = false;
        }
    }
}
